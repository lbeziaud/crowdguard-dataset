from lxml import etree
from arango import ArangoClient
import re

client = ArangoClient(
    protocol='http',
    host='localhost',
    port=8529,
    username='root',
    password='7OseFmHO4JyRQG4KN0ELGy'
)

db = client.create_database('se_english')

tags = db.create_collection('tags')
# tags.add_hash_index(fields=['name'], unique=True)

posts = db.create_collection('posts')
# posts.add_hash_index(fields=['id'], unique=True)

corpus = db.create_graph('corpus')
has = corpus.create_edge_definition(
    name='has',
    from_collections=['tags'],
    to_collections=['posts']
)

tags_fn = '../english.stackexchange.com/Tags.xml'
for event, element in etree.iterparse(tags_fn, tag='row'):
    tag_name = element.attrib.get('TagName')

    tags.insert({'_key': tag_name})

    element.clear()
    while element.getprevious() is not None:
        del element.getparent()[0]

r_tag = re.compile('<(.*?)>')
posts_fn = '../english.stackexchange.com/Posts.xml'
for event, element in etree.iterparse(posts_fn, tag='row'):
    post_type = element.attrib.get('PostTypeId')
    if post_type != '1':
        # not a question
        continue

    post_id = element.attrib.get('Id')
    post_title = element.attrib.get('Title')
    post_body = element.attrib.get('Body')
    post_score = int(element.attrib.get('Score'))

    posts.insert({'_key': post_id, 'title': post_title,
                  'body': post_body, 'score': post_score})

    post_tags = element.attrib.get('Tags')
    for tag_name in r_tag.findall(post_tags):
        has.insert({
            '_from': 'tags/{}'.format(tag_name),
            '_to': 'posts/{}'.format(post_id)
        })

    element.clear()
    while element.getprevious() is not None:
        del element.getparent()[0]


