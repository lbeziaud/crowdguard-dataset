from lxml import etree
from arango import ArangoClient
import re
import html
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import networkx as nx

client = ArangoClient(
    protocol='http',
    host='localhost',
    port=8529,
    username='root',
    password='7OseFmHO4JyRQG4KN0ELGy'
)

db = client.db('se_english')

cursor = db.aql.execute(
    """
    FOR t IN tags
        FILTER t._key != "ROOT"
        RETURN {
            "tag": t._id,
            "posts": CONCAT_SEPARATOR(", ", (
                FOR h IN has
                    FILTER h._from == t._id
                    FOR p IN posts
                        FILTER p._id == h._to
                        SORT p.score DESC
                        RETURN CONCAT_SEPARATOR(", ", p.title, p.body)
            ))
        }
    """
)

corpus = pd.DataFrame(((row['tag'], row['posts']) for row in cursor), columns=['tag', 'posts'])

r_tag = re.compile('<(.*?)>')
corpus.posts = corpus.posts.apply(lambda p: re.sub(r_tag, '', html.unescape(p)))

tf = TfidfVectorizer(analyzer='word', stop_words='english')
tfidf = tf.fit_transform(corpus.posts)

csim = linear_kernel(tfidf, tfidf)
csim = pd.DataFrame(csim, columns=corpus.tag, index=corpus.tag)

def heymann2006collaborative(threshold):
    tags = corpus.tag
    taxonomy = nx.DiGraph()
    for ti in tags:
        # find most outer node
        max_candidate = None
        max_candidate_val = -1
        for tj in taxonomy:
            if tj == 'tags/ROOT': continue
            sim = csim[ti][tj]
            if sim > max_candidate_val:
                max_candidate_val = sim
                max_candidate = tj
        if max_candidate_val > threshold:
            taxonomy.add_edge(max_candidate, ti)
        else:
            taxonomy.add_edge('tags/ROOT', ti)
    return taxonomy

threshold = 0.4
taxonomy = heymann2006collaborative(threshold)

is_a = db.graph('taxonomy').edge_collection('is_a')

for u, v in taxonomy.edges():
    print(u, v)
    is_a.insert({
        '_from': u,
        '_to': v
    })
