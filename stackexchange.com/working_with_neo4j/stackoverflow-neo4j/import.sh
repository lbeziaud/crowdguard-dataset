NEO_ROOT="../neo4j-community-3.2.0/"

rm -rf $NEO_ROOT/data/graph.db

$NEO_ROOT/bin/neo4j-admin import \
                          --id-type string \
                          --nodes:Post csvs/posts.csv \
                          --nodes:Tag csvs/tags.csv \
                          --relationships:PARENT_OF csvs/posts_rel.csv \
                          --relationships:HAS_TAG csvs/tags_posts_rel.csv \
                          --ignore-missing-nodes
