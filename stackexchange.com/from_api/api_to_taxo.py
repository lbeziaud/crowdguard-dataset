import pandas as pd
import html
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import numpy as np
import networkx as nx
from networkx.drawing.nx_agraph import write_dot


# load documents
docs1 = pd.read_csv('docs.csv', header=0, names=['tag', 'doc'])
docs2 = pd.read_csv('docs2.csv', header=0, names=['tag', 'doc'])
docs = pd.concat([docs1, docs2])

# unescape html (eg. &lt; $\to$ <)
docs.doc = docs.doc.apply(html.unescape)
# remove html tags
cleanr = re.compile('<.*?>')
docs.doc = docs.doc.apply(lambda doc: re.sub(cleanr, '', doc))
# compute tf-idf vectors
tf = TfidfVectorizer(analyzer='word', stop_words='english')
tfidf = tf.fit_transform(docs.doc)
# compute cosine similarities
cosine_sim = linear_kernel(tfidf, tfidf)
msim = pd.DataFrame(cosine_sim, columns=docs.tag, index=docs.tag)

def heymann2006collaborative(threshold):
    tags = docs.tag  # set of tags
    taxonomy = nx.DiGraph()  # directed graph
    taxonomy.add_node('root')
    for ti in tags:
        # find most outer node
        max_candidate = None
        max_candidate_val = -1
        for tj in taxonomy:
            if tj == 'root': continue
            sim = msim[ti][tj]
            if sim > max_candidate_val:
                max_candidate_val = sim
                max_candidate = tj
        # add node to the taxonomy
        if max_candidate_val > threshold:
            taxonomy.add_edge(max_candidate, ti)
        else:
            taxonomy.add_edge('root', ti)
    return taxonomy


threshold = 0
taxonomy = heymann2006collaborative(threshold)
write_dot(taxonomy, 'taxonomy.dot')
