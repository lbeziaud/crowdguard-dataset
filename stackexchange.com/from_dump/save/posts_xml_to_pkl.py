# - Id
# - PostTypeId
#    - 1: Question
#    - 2: Answer
# - ParentID (only present if PostTypeId is 2)
# - AcceptedAnswerId (only present if PostTypeId is 1)
# - CreationDate
# - Score
# - ViewCount
# - Body
# - OwnerUserId
# - LastEditorUserId
# - LastEditorDisplayName="Jeff Atwood"
# - LastEditDate="2009-03-05T22:28:34.823"
# - LastActivityDate="2009-03-11T12:51:01.480"
# - CommunityOwnedDate="2009-03-11T12:51:01.480"
# - ClosedDate="2009-03-11T12:51:01.480"
# - Title=
# - Tags=
# - AnswerCount
# - CommentCount
# - FavoriteCount

import re
import sys
import lxml.etree as ET
from bs4 import BeautifulSoup
import pickle


r = re.compile('<(.*?)>')

def parse_posts(fn):
    i = 0
    n = 1000
    for event, elem in ET.iterparse(fn):
        if event == 'end':
            if elem.tag == 'row':

                type = elem.get('PostTypeId')
                score = int(elem.get('Score'))
                if type == '1' and score > 0:

                    id = elem.get('Id')
                    body = BeautifulSoup(elem.get('Body'), 'lxml').get_text()
                    tags = r.findall(elem.get('Tags', ''))

                    post = {'id': elem.get('Id'),
                            'score': int(elem.get('Score')),
                            'body': BeautifulSoup(elem.get('Body'), 'lxml').get_text(),
                            'tags': r.findall(elem.get('Tags', ''))}

                    if i % n == 0: print(i)
                    i += 1

                    yield post
        elem.clear()
    print()

if __name__ == "__main__":
    dir = sys.argv[1] + '/'
    posts = list(parse_posts(dir + 'Posts.xml'))
    with open(dir + 'Posts.pkl', 'wb') as f:
        pickle.dump(posts, f, pickle.HIGHEST_PROTOCOL)
