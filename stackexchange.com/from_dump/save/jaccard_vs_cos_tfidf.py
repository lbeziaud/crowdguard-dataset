import lxml.etree as ET
from operator import itemgetter
import os
import pickle
import numpy as np
from collections import defaultdict
import networkx as nx
import re
import functools
import sys
import glob
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

with open('../1-1000.txt', 'r') as f:
    lines = f.readlines()
    stopwords = set(l.strip().lower() for l in lines)

def jaccard(set1, set2):
    intn = set1.intersection(set2)
    unin = set1.union(set2)
    if len(unin) == 0:
        return 0.0
    else:
        return len(intn) / len(unin)
    
def parse_posts(fn):
    posts = []
    r = re.compile('<(.*?)>')
    for event, elem in ET.iterparse(fn):
        if event == 'end':
            if elem.tag == 'row':
                postScore = elem.get('Score')
                if postScore[0] == '-':
                    continue # ignore if negative score

                postTypeId = elem.get('PostTypeId')
                if postTypeId != '1':
                    continue # ignore if not a question

                postBody = elem.get('Body')
                postBody = BeautifulSoup(postBody, 'lxml').get_text()
                postBody = postBody.strip().lower()

                postWords = ''.join(c for c in postBody if c.isalpha() or c.isspace())
                postWords = postWords.split()
                postWords = [w for w in postWords if len(w) >= 2]
                postWords = set(postWords) - stopwords
                
                postId = elem.get('Id')

                postTags = r.findall(elem.get('Tags', '')) # split tags
                postTags = set(postTags)
                
                posts.append({'tags': postTags, 'words': postWords, 'body': postBody})

        elem.clear()

    n = len(posts)

    vect = TfidfVectorizer(min_df=1, stop_words=None)
    tfidf = vect.fit_transform([post['body'] for post in posts])
    body_cos = (tfidf * tfidf.T).A

    vect = TfidfVectorizer(min_df=1, stop_words=None)
    tfidf = vect.fit_transform([' '.join(post['words']) for post in posts])
    words_cos = (tfidf * tfidf.T).A
    
    for i, j in zip(*np.triu_indices(n, 1)):
        post_i = posts[i]
        post_j = posts[j]

        tags_jacc_sim = jaccard(post_i['tags'], post_j['tags'])
        words_jacc_sim = jaccard(post_i['words'], post_j['words'])
        words_cos_sim = words_cos[i, j]
        body_cos_sim = body_cos[i, j]

        yield ({'tags_jacc_sim': tags_jacc_sim,
                'words_jacc_sim': words_jacc_sim,
                'words_cos_sim': words_cos_sim,
                'body_cos_sim': body_cos_sim})

# for fn in glob.iglob('*/Posts.xml'):
#     dirname = os.path.dirname(fn)
#     parse_posts(fn)

res = parse_posts('languagelearning.stackexchange.com/Posts.xml')

df = pd.DataFrame(res)
print(df)

# df = df[(df.tagssim > 0) & (df.wordssim > 0)]

sns.jointplot(x='tags_jacc_sim', y='words_jacc_sim', data=df, kind='reg')
sns.jointplot(x='tags_jacc_sim', y='words_cos_sim', data=df, kind='reg')
sns.jointplot(x='tags_jacc_sim', y='body_cos_sim', data=df, kind='reg')

plt.show()
