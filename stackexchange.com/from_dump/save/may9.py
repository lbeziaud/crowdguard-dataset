import html
import re
from collections import defaultdict
from queue import Queue
from threading import Thread
from lxml import etree
import glob
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import os
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout

dir = 'math.stackexchange.com/'

# splittags = re.compile('<(.*?)>')
# out = {}
# for event, element in etree.iterparse(dir + 'Posts.xml', tag='row'):
#     body = element.attrib.get('Body', '')
#     title = element.attrib.get('Title', '')
#     tags = splittags.findall(element.attrib.get('Tags', ''))
#     for tag in tags:
#         if tag not in out:
#             out[tag] = open(dir + tag + '.raw', 'w')
#         out[tag].write(body + '\n')
#         out[tag].write(title + '\n')
#     element.clear()
# for tag, f in out.items():
#     f.close()

# cleanr = re.compile('<.*?>')
# for fn in glob.glob(dir + '*.raw'):
#     with open(fn, 'r') as f:
#         new_fn = fn[:-4] + '.txt'
#         with open(new_fn, 'w') as new_f:
#             lines = f.readlines()
#             for line in lines:
#                 line = html.unescape(line)
#                 line = re.sub(cleanr, '', line)
#                 new_f.write(line)

# corpus = glob.glob(dir + '*.txt')
# tf = TfidfVectorizer(analyzer='word', stop_words='english')
# tfidf = tf.fit_transform(corpus)
# with open(dir + 'vectorizer.pk', 'wb') as f:
#     pickle.dump(tfidf, f)
# print(tf.get_feature_names())

with open(dir + 'vectorizer.pk', 'rb') as f:
    tfidf = pickle.load(f)
# compute cosine similarities
cosine_sim = linear_kernel(tfidf, tfidf)

dist = 1 - cosine_sim

G = nx.from_numpy_matrix(dist)
nx.set_node_attributes(G, 'name', dict(enumerate((os.path.basename(fn)[:-4] for fn in glob.glob(dir + '*.txt')))))

T = nx.minimum_spanning_tree(G)

# fig, ax = plt.subplots()
# pos = graphviz_layout(T)
# nx.draw_networkx_labels(T, pos=pos, labels={i: d['name'] for i, d in T.nodes(data=True)}, font_size=12)
# nx.draw(T, pos=pos, ax=ax)
# plt.show()
