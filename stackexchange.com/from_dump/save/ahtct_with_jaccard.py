import numpy as np
from collections import defaultdict
import lxml.etree as ET
import networkx as nx
import re
import functools
import sys

# se = 'languagelearning.stackexchange.com/'

import time
start_time = time.time()

se = sys.argv[1] + '/'

fn = se + 'Posts.xml'

def parse_posts(fn):
    count = 0
    tags = defaultdict(set)
    r = re.compile('<(.*?)>')
    for event, elem in ET.iterparse(fn):
        if event == 'end':
            if elem.tag == 'row':
                postScore = elem.get('Score')
                if postScore[0] != '-': # score is not negative
                    postId = elem.get('Id')
                    postTags = r.findall(elem.get('Tags', '')) # split tags
                    for tag in postTags:
                        tags[tag].add(postId)
                    count += 1
        elem.clear()
    return tags, count

tags, count = parse_posts(fn)

@functools.lru_cache(maxsize=None)
def proximity(t1, t2):
    return len(tags[t1].intersection(tags[t2]))

def ahctc(D):
    n = len(D)
    # T = np.empty((n, n), dtype=np.boo;)
    T = {t1: {t2: 0 for t2 in D} for t1 in D}
    M = [[d] for d in D]

    # @lru_cache(maxsize=None)
    def sup(mi):
        di_min = None
        prox_min = np.inf
        for di in mi:
            prox = 0
            for dj in mi:
                prox += proximity(di, dj)
            if prox < prox_min:
                prox_min = prox
                di_min = di
        return di_min

    while len(M) > 1:

        i_max, j_max = None, None
        prox_max = -1
        for i in range(len(M)):
            for j in range(i + 1, len(M)):
                prox = proximity(sup(M[i]), sup(M[j]))
                if prox > prox_max:
                    prox_max = prox
                    i_max = i
                    j_max = j
        mi = M[i_max]
        mj = M[j_max]

        m = mi + mj
        M.append(m)
        M.remove(mi)
        M.remove(mj)

        supm = sup(m)
        supmi = sup(mi)
        supmj = sup(mj)

        if supm == supmi:
            T[supm][supmj] = 1
        elif supm == supmj:
            T[supm][supmi] = 1
        else:
            k = next(k for k in D if T[k][supm] == 1)
            T[k][supm] = 0
            for g in (g for g in D if T[supm][g] == 1):
                T[supm][g] = 0
                T[k][g] = 1
            T[supm][supmi] = 1
            T[supm][supmj] = 1

    return T

# import cProfile
# cProfile.run('ahctc(tags)')

T = ahctc(tags)

G = nx.DiGraph()
for t1 in T:
    for t2 in T:
        if T[t1][t2]:
            G.add_edge(t1, t2)

nx.drawing.nx_agraph.write_dot(G, se + 'ahctc.dot')

print(se, count, time.time() - start_time)

# pos = graphviz_layout(G)
# nx.draw(G,pos=pos)
# plt.show()
