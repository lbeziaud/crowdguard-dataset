import networkx as nx


def algo1(tags, similarity):
    """
    http://ilpubs.stanford.edu:8090/775/1/2006-10.pdf
    """
    taxonomy = nx.DiGraph()
    taxonomy.add_node('root')
    for ti in tags:
        maxCandidate = None
        maxCandidateVal = 0
        for tj in taxonomy:
            sim = similarity(ti, tj)
            if sim > maxCandidateVal:
               maxCandidateVal = sim
               maxCandidate = tj
        if maxCandidateVal > taxThreshold:
            taxonomy.add_edge(maxCandidate, ti)
        else:
            taxonomy.add_edge('root', ti)
    return taxonomy

def ahctc(tags):
    """
    https://yuyue.github.io/res/paper/ADMA12-camera-ready.pdf
    """
    n = len(tags)
    taxonomy = nx.DiGraph()
    clusters = [[tag] for tag in tags]

    def sup(mi):
        di_min = None
        prox_min = np.inf
        for di in mi:
            prox = 0
            for dj in mi:
                prox += proximity(di, dj)
            if prox < prox_min:
                prox_min = prox
                di_min = di
        return di_min

    while len(clusters) > 1:

        i_max, j_max = None, None
        prox_max = -1
        for i in range(len(clusters)):
            for j in range(i + 1, len(clusters)):
                prox = proximity(sup(clusters[i]), sup(clusters[j]))
                if prox > prox_max:
                    prox_max = prox
                    i_max = i
                    j_max = j
        mi = clusters[i_max]
        mj = clusters[j_max]

        m = mi + mj
        clusters.append(m)
        clusters.remove(mi)
        clusters.remove(mj)

        supm = sup(m)
        supmi = sup(mi)
        supmj = sup(mj)

        if supm == supmi:
            taxonomy.add_edge(supm, supmj)
        elif supm == supmj:
            taxonomy.add_edge(supm, supmi)
        else:
            k = next(k for k in D if taxonomy.has_edge(k, supm))
            taxonomy.remove_edge(k, supm) = 0
            for g in (g for g in D if taxonomy.has_edge(supm, g)):
                taxonomy.remove_edge(supm, g) = 0
                taxonomy.add_edge(k, g) = 1
            taxonomy.add_edge(supm, supmi) = 1
            taxonomy.add_edge(supm, supmj) = 1

    return T

algo1([1, 2, 3])
