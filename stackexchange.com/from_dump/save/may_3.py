from lxml import etree
import os

def parse(fn):
    for event, element in etree.iterparse(fn, tag='row'):
        title = element.attrib.get('Title', '')
        element.clear()

def get_chunks(file_size):
    chunk_start = 0
    chunk_size = 0x20000  # 131072 bytes, default max ssl buffer size
    while chunk_start + chunk_size < file_size:
        yield(chunk_start, chunk_size)
        chunk_start += chunk_size

    final_chunk_size = file_size - chunk_start
    yield(chunk_start, final_chunk_size)

def read_file_chunked(file_path):
    with open(file_path) as file_:
        file_size = os.path.getsize(file_path)

        print('File size: {}'.format(file_size))

        progress = 0

        for chunk_start, chunk_size in get_chunks(file_size):

            file_chunk = file_.read(chunk_size)

            parse(file_chunk)

            progress += len(file_chunk)
            # print('{0} of {1} bytes read ({2}%)'.format(
            #     progress, file_size, int(progress / file_size * 100))
            # )

if __name__ == '__main__':
    read_file_chunked('math.stackexchange.com/Posts.xml')
