import re
from lxml import etree
from bs4 import BeautifulSoup
import MySQLdb
import itertools
from nltk.corpus import stopwords
from gensim.utils import simple_preprocess
from gensim import corpora, models, similarities, matutils
from collections import defaultdict, Counter
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import networkx as nx
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import json
from networkx.readwrite import json_graph
from networkx.drawing.nx_agraph import  write_dot

db = MySQLdb.connect(host='localhost', user='root', passwd='f6QJeo({,xT~OH6})E=^;.', db='stackoverflow')
db.set_character_set('utf8')
cur = db.cursor()
cur.execute('SET GLOBAL max_allowed_packet=1073741824')

numTags = 100
numPosts = 50

categories = []
documents = []

cur.execute('SELECT id, tag_name, count(post_id) FROM tags INNER JOIN post_tags ON tags.id=post_tags.tag_id GROUP BY id ORDER BY count(post_id) DESC LIMIT %s', (numTags,))
for (tag_id, tag_name, tag_count) in cur.fetchall():
    # print(tag_id, tag_name, tag_count)
    document = ""
    cur.execute('SELECT posts.id, body FROM posts INNER JOIN post_tags ON posts.id=post_tags.post_id WHERE post_tags.tag_id=%s AND score>0 LIMIT %s;', (tag_id, numPosts))
    for (post_id, post_body) in cur.fetchall():
        body = BeautifulSoup(post_body, 'lxml').get_text()
        document += body
        # tokens = [token for token in simple_preprocess(body) if token not in stoplist]
        # document += tokens
    categories.append(tag_name)
    documents.append(document)
    # texts.append(document)

# remove common words and tokenize
stoplist = set(stopwords.words('english'))
texts = [[token for token in simple_preprocess(document) if token not in stoplist] for document in documents]

# remove words that appear only once
frequency = Counter()
for text in texts:
    frequency.update(text)
texts = [[token for token in text if frequency[token] > 1] for text in texts]


dictionary = corpora.Dictionary(texts)
dictionary.save('/tmp/english.dict')

corpus = [dictionary.doc2bow(text) for text in texts]
corpora.MmCorpus.serialize('/tmp/english.mm', corpus)
# corpus = corpora.MmCorpus('/tmp/corpus.mm') # load corpus

tfidf = models.TfidfModel(corpus)
corpus_tfidf = tfidf[corpus]
index = similarities.MatrixSimilarity(tfidf[corpus])
index.save('/tmp/english.index')
# index = similarities.MatrixSimilarity.load('/tmp/english.index') # load index

sims = index[corpus_tfidf]

def similarity(t1, t2):
    i1 = categories.index(t1)
    i2 = categories.index(t2)
    sim = sims[i1][i2]
    return sim

tags = [t for t in categories]
taxThreshold = 0.099

# http://ilpubs.stanford.edu:8090/775/1/2006-10.pdf
taxonomy = nx.DiGraph()
taxonomy.add_node('root')
for ti in tags:
    maxCandidate = None
    maxCandidateVal = 0
    for tj in taxonomy:
        if tj == 'root':
            continue
        sim = similarity(ti, tj)
        if sim > maxCandidateVal:
           maxCandidateVal = sim
           maxCandidate = tj
    if maxCandidateVal > taxThreshold:
        taxonomy.add_edge(maxCandidate, ti)
    else:
        taxonomy.add_edge('root', ti)

# draw graph
# pos = graphviz_layout(taxonomy)
# nx.draw_networkx(taxonomy, pos)
# plt.show()

# for n, d in taxonomy.nodes(data=True):
#     d['name'] = n
# with open('taxonomy.json', 'w') as f:
#     f.write(json.dumps(json_graph.tree_data(taxonomy, root="root")))

write_dot(taxonomy, 'taxonomy.dot')

# # brooks2006improved algo6
# tags = [set([t]) for t in range(len(categories))]
# while len(tags) > 1:
#     ti, tj = 0, 0
#     max_sim = -1
#     for tx in tags:
#         for ty in tags:
#             if tx != ty:
#                 if sim(tx, ty) > max_sim:
#                     max_sim = sim(tx, ty)
#                     ti = tx
#                     tj = ty
#     tnew = ti ti, tj]
