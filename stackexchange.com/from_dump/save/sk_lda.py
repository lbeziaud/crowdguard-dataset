import sys
import lxml.etree as ET
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF
import re

fn = sys.argv[1]

corpus = []
tagged = []

n = 6000
i = 0

r = re.compile('<(.*?)>')
for event, elem in ET.iterparse(fn):
    if event == 'end':
        if elem.tag == 'row':
            type = elem.get('PostTypeId')
            score = elem.get('Score')
            if score[0] != '-' and type == '1': # positive score + question
                body = BeautifulSoup(elem.get('Body'), 'lxml').get_text()
                tags = r.findall(elem.get('Tags', ''))
                # body += ' ' + ' '.join(tags)
                corpus.append(body)
                tagged += tags

                if i > n: break
                else: i += 1
    elem.clear()

n_topics = 15
n_top_words = 50
n_features = 6000

# vectorizer = TfidfVectorizer(analyzer='word', ngram_range=(1,1), min_df = 0, stop_words = 'english')
vectorizer = CountVectorizer(analyzer='word', ngram_range=(1,1), min_df = 0, stop_words = 'english')
matrix =  vectorizer.fit_transform(corpus)
feature_names = vectorizer.get_feature_names()

import lda
import numpy as np

vocab = feature_names

model = lda.LDA(n_topics=n_topics, n_iter=500, random_state=1)
model.fit(matrix)
topic_word = model.topic_word_
n_top_words = 20

for i, topic_dist in enumerate(topic_word):
    topic_words = np.array(vocab)[np.argsort(topic_dist)][:-n_top_words:-1]
    print('Topic {}: {}'.format(i, ' '.join(topic_words)))

doc_topic = model.doc_topic_
for i in range(0, len(tagged)):
    print("{} (top topic: {})".format(tagged[i], doc_topic[i].argmax()))
    print(doc_topic[i].argsort()[::-1][:3])
