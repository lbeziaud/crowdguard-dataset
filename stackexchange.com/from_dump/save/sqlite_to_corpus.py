import lxml.etree as ET
import re
import sys
import sqlite3
import os
from bs4 import BeautifulSoup

dir = 'english.stackexchange.com'

def posts2db(dbname):

conn = sqlite3.connect('{}/posts.db'.format(dir))

c = conn.cursor()

numTags = 100
numPosts = 50

c.execute('SELECT id, name, count(postId) FROM tag INNER JOIN post2tag ON tag.id=post2tag.tagId GROUP BY id ORDER BY count(postId) DESC LIMIT ?', (numTags,))
for (tagId, tagName, tagCount) in c.fetchall():
    tagDir = '{}/corpus/{}'.format(dir, tagName)
    if not os.path.exists(tagDir):
        os.makedirs(tagDir)

    c.execute('SELECT post.id, body FROM post INNER JOIN post2tag ON post.id=post2tag.postId WHERE post2tag.tagId=? AND score>0 ORDER BY score DESC LIMIT ?;', (tagId, numPosts))
    for (postId, postBody) in c.fetchall():
        body = BeautifulSoup(postBody, 'lxml').get_text()
        with open('{}/{}.txt'.format(tagDir, postId), 'w') as f:
            f.write(body)

conn.close()
