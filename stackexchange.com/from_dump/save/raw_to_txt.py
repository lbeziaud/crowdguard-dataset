import argparse
import re
import os
import glob
import html

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input', help='input dir (*.raw)', required=True)
parser.add_argument('-o', '--output', help='output dir', required=True)

args = parser.parse_args()

outputs = dict()

cleanr = re.compile('<.*?>')

for in_fn in glob.glob(os.path.join(args.input, '*.raw')):
    with open(in_fn, 'r') as in_f:
        base = os.path.basename(in_fn)
        tag_name = os.path.splitext(base)[0]
        out_fn = os.path.join(args.output, tag_name + '.txt')
        with open(out_fn, 'w') as out_f:
            lines = in_f.readlines()
            for line in lines:
                line = html.unescape(line) # unescape html &lt;
                line = re.sub(cleanr, '', line) # remove html <tags>
                out_f.write(line)
