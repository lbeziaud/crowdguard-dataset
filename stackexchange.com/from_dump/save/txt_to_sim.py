import argparse
import os
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import glob
import pandas as pd

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input', help='input dir (*.txt)', required=True)
parser.add_argument('-o', '--output', help='output file', required=True)

args = parser.parse_args()

corpus = glob.glob(os.path.join(args.input, '*.txt'))

tags = [os.path.splitext(os.path.basename(f))[0] for f in corpus]

tf = TfidfVectorizer(analyzer='word', stop_words='english')
tfidf = tf.fit_transform(corpus)

cosine_sim = linear_kernel(tfidf, tfidf)

df = pd.DataFrame(cosine_sim, columns=tags, index=tags)

fn = os.path.join(args.output, 'cosine_sim.pkl')
df.to_pickle(fn)
