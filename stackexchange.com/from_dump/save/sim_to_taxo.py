import networkx as nx
import json
from networkx.readwrite import json_graph
import argparse
import pandas as pd

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input', help='input file (cosine_sim.pkl)', required=True)
parser.add_argument('-o', '--output', help='output file', required=True)
parser.add_argument('-t', '--threshold', help='threshold', required=True, type=float)

args = parser.parse_args()

def heymann2006collaborative(threshold, msim):
    tags = msim.columns  # set of tags
    taxonomy = nx.DiGraph()  # directed graph
    taxonomy.add_node('root')
    for ti in tags:
        # find most outer node
        max_candidate = None
        max_candidate_val = -1
        for tj in taxonomy:
            if tj == 'root': continue
            sim = msim[ti][tj]
            if sim > max_candidate_val:
                max_candidate_val = sim
                max_candidate = tj
        # add node to the taxonomy
        if max_candidate_val > threshold:
            taxonomy.add_edge(max_candidate, ti)
        else:
            taxonomy.add_edge('root', ti)
    taxonomy = nx.convert_node_labels_to_integers(taxonomy, label_attribute='name')
    print(taxonomy.nodes(data=True))
    return taxonomy


threshold = args.threshold
msim = pd.read_pickle(args.input)

taxonomy = heymann2006collaborative(threshold, msim)

root = next((n for n in taxonomy if taxonomy.node[n]['name'] == 'root')) # TODO: opti

js = json_graph.tree_data(taxonomy, root=root)
with open(args.output, 'w') as f:
    f.write(json.dumps(js))
