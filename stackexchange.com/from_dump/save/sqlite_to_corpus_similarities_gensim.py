import lxml.etree as ET
import re
import sys
import sqlite3
import os
from bs4 import BeautifulSoup
from gensim.utils import simple_preprocess
from gensim import corpora, models, similarities
from nltk.corpus import stopwords

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

dir = 'english.stackexchange.com'

dbname = '{}/english.db'.format(dir)
numTags = 100
numPosts = 50


def fetch_documents(numTags, numPosts, dbname):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    stoplist = set(stopwords.words('english'))
    texts = []
    c.execute('SELECT id, name, count(postId) FROM tag INNER JOIN post2tag ON tag.id=post2tag.tagId GROUP BY id ORDER BY count(postId) DESC LIMIT ?', (numTags,))
    for (tagId, tagName, tagCount) in c.fetchall():
        document = []
        c.execute('SELECT post.id, body FROM post INNER JOIN post2tag ON post.id=post2tag.postId WHERE post2tag.tagId=? AND score>0 ORDER BY score DESC LIMIT ?;', (tagId, numPosts))
        for (postId, postBody) in c.fetchall():
            body = BeautifulSoup(postBody, 'lxml').get_text()
            tokens = [token for token in simple_preprocess(body) if token not in stoplist]
            document += tokens
        texts.append(document)
    return texts

texts = fetch_documents(numTags, numPosts, dbname)

dictionary = corpora.Dictionary(corpus)
dictionary.save('/tmp/tweets.dict') # store the dictionary, for future reference

index = similarities.docsim.MatrixSimilarity(corpus)
print(index)
