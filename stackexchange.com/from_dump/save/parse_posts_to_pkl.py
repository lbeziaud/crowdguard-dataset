import lxml.etree as ET
from operator import itemgetter
import os
import pickle
import numpy as np
from collections import defaultdict
import networkx as nx
import re
import functools
import sys
import glob


def parse_posts(fn):
    count = 0
    tags = defaultdict(set)
    r = re.compile('<(.*?)>')
    for event, elem in ET.iterparse(fn):
        if event == 'end':
            if elem.tag == 'row':
                postScore = elem.get('Score')
                if postScore[0] == '-':
                    continue # ignore if negative score

                postTypeId = elem.get('PostTypeId')
                if postTypeId != '1':
                    continue # ignore if not a question

                postId = elem.get('Id')
                postTags = r.findall(elem.get('Tags', '')) # split tags
                for tag in postTags:
                    tags[tag].add(postId)
                count += 1

        elem.clear()
    return tags, count


sites = []
    

for fn in glob.iglob('*/Posts.xml'):
    dirname = os.path.dirname(fn)
    
    tags2posts, goodquestions = parse_posts(fn)

    tags = len(tags2posts)
    
    with open(dirname + '/tags2posts.pkl', 'wb') as output:
        pickle.dump(tags2posts, output, pickle.HIGHEST_PROTOCOL)

    print('{},{},{}'.format(dirname,goodquestions,tags))
