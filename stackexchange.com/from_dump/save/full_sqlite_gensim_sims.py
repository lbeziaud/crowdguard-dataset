import lxml.etree as ET
import re
import sys
import sqlite3
import os
from nltk.corpus import stopwords
from gensim.utils import simple_preprocess
from bs4 import BeautifulSoup
from gensim import corpora, models, similarities


FN_TAGS = 'english.stackexchange.com/Tags.xml'
FN_POSTS = 'english.stackexchange.com/Posts.xml'
FN_DB  = 'english.stackexchange.com/english.db'
FN_DOCS = 'english.stackexchange.com/english.txt'
FN_CAT = 'english.stackexchange.com/english.cat'
FN_CORPUS = 'english.stackexchange.com/english.mm'
FN_DICT = 'english.stackexchange.com/english.dict'
FN_INDEX = 'english.stackexchange.com/english.index'
FN_TFIDF = 'english.stackexchange.com/english.corpus_tfidf'

NUM_TAGS = 100
NUM_POSTS = 50


print('build db')
# conn = sqlite3.connect(FN_DB)
# c = conn.cursor()
# c.execute('CREATE TABLE IF NOT EXISTS tag (id INTEGER PRIMARY KEY, name TEXT)')
# c.execute('CREATE TABLE IF NOT EXISTS post (id INTEGER PRIMARY KEY, score INTEGER, body TEXT)')
# c.execute('CREATE TABLE IF NOT EXISTS post2tag (postId INTEGER, tagId INTEGER, PRIMARY KEY (postId, tagId), FOREIGN KEY(postId) REFERENCES post(id), FOREIGN KEY(tagId) REFERENCES tag(id))')
# r = re.compile('<(.*?)>')
# for event, elem in ET.iterparse(FN_TAGS):
#     if event == 'end':
#         if elem.tag == 'row':
#             tagId = elem.get('Id')
#             tagName = elem.get('TagName')
#             c.execute('INSERT OR REPLACE INTO tag VALUES (?, ?)', (tagId, tagName))
#     elem.clear()
# conn.commit()
# for event, elem in ET.iterparse(FN_POSTS):
#     if event == 'end':
#         if elem.tag == 'row':
#             if elem.get('PostTypeId') != '1':
#                 continue
#             postId = elem.get('Id')
#             score = elem.get('Score')
#             body = elem.get('Body')
#             c.execute('INSERT OR REPLACE INTO post VALUES (?, ?, ?)', (postId, score, body))
#             for tagName in r.findall(elem.get('Tags', '')):
#                 c.execute('SELECT id FROM tag WHERE name = ?', (tagName,))
#                 tagId = c.fetchone()[0]
#                 c.execute('INSERT OR REPLACE INTO post2tag VALUES (?, ?)', (postId, tagId))
#     elem.clear()
# conn.commit()
# conn.close()


print('fetch documents')
# stoplist = set(stopwords.words('english'))
# conn = sqlite3.connect(FN_DB)
# c = conn.cursor()
# features = []
# c.execute('SELECT id, name, count(postId) FROM tag INNER JOIN post2tag ON tag.id=post2tag.tagId GROUP BY id ORDER BY count(postId) DESC LIMIT ?', (NUM_TAGS,))
# for (tagId, tagName, tagCount) in c.fetchall():
#     features.append(tagName)
#     document = []
#     c.execute('SELECT body FROM post INNER JOIN post2tag ON post.id=post2tag.postId WHERE post2tag.tagId=? AND score>=0 ORDER BY score DESC LIMIT ?;', (tagId, NUM_POSTS))
#     for (postBody,) in c.fetchall():
#         body = BeautifulSoup(postBody, 'lxml').get_text()
#         tokens = [token for token in simple_preprocess(body) if token not in stoplist]
#         document += tokens
#     with open(FN_DOCS, 'w') as f:
#         f.write(' '.join(document) + '\n')
# with open(FN_CAT, 'w') as f:
#     f.write(','.join(features) + '\n')
# conn.close()


print('build dictionary')
# dictionary = corpora.Dictionary(line.lower().split() for line in open(FN_DOCS, 'r'))
# dictionary.save(FN_DICT)
dictionary = corpora.Dictionary.load(FN_DICT)
print(dictionary)


print('build corpus')
# class MyCorpus(object):
#     def __iter__(self):
#         for line in open(FN_DOCS):
#             yield dictionary.doc2bow(line.lower().split())
# corpus = MyCorpus()
# corpora.MmCorpus.serialize(FN_CORPUS, corpus)
corpus = corpora.MmCorpus(FN_CORPUS)
print(corpus)


print('compute tfidf')
tfidf = models.TfidfModel(corpus)
corpus_tfidf = tfidf[corpus]
# corpus_tfidf.save(FN_TFIDF)


print('compute index')
index = similarities.MatrixSimilarity(tfidf[corpus])
index.save(FN_INDEX)


print('compute sims')
sims = index[corpus_tfidf]
print(sims)
