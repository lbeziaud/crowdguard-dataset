import numpy as np
import pandas as pd
import xml.etree.ElementTree as ET
import re
from scipy.spatial.distance import hamming
from scipy.cluster import hierarchy
import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist
from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import dendrogram
from sklearn.cluster import AgglomerativeClustering
from collections import defaultdict

np.set_printoptions(threshold=np.nan)

se = 'languagelearning.stackexchange.com/'

# tags = {child.attrib['TagName']: i for i, child in enumerate(ET.parse(se + 'Tags.xml').getroot())}

tags = defaultdict(set)

for post in ET.parse(se + 'Posts.xml').getroot():
    postId = int(post.attrib['Id'])
    if 'Tags' in post.attrib:
        postTags = post.attrib['Tags'].replace('<', '').split('>')[:-1]
        for tag in postTags:
            tags[tag].add(postId)

n = len(tags)
inter = np.empty((n, n))
for i, (tag1, posts1) in enumerate(tags.items()):
    for j, (tag2, posts2) in enumerate(tags.items()):
        similarity = len(posts1.intersection(posts2))
        inter[i, j] = similarity

# X = np.array([[similarities[tag1][tag2] for tag1 in sorted(tags)] for author in sorted(tags)])
# Y = (X / X.max())
# plt.imshow(Y)
# plt.show()
# Z = linkage(Y)
# root = hierarchy.to_tree(Z)

import networkx as nx

# G = nx.from_numpy_matrix(inter, parallel_edges=True, create_using=nx.MultiGraph())
G = nx.from_numpy_matrix(inter)
print(G[0][0])

pos = nx.spring_layout(G, weight='weight', k=2/np.sqrt(n))

labels=nx.draw_networkx_labels(G,pos=pos, labels={i: name for i, name in enumerate(tags)})
edges=nx.draw_networkx_edges(G, pos=pos, width=[d['weight'] for (u,v,d) in G.edges(data=True)])
nx.draw(G,pos=pos)

plt.show()
