CREATE DATABASE stackoverflow CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE stackoverflow;

CREATE TABLE posts (
    id INT PRIMARY KEY,
    post_type_id TINYINT,
    parent_id INT,
    accepted_answer_id INT,
    score INT,
    view_count INT,
    body TEXT,
    title NVARCHAR(250),
    answer_count INT,
    comment_count INT,
    favorite_count INT
); 

CREATE TABLE tags (
    id INT PRIMARY KEY,
    tag_name NVARCHAR(35)
);

CREATE TABLE post_tags (
    post_id INT,
    tag_id INT,
    PRIMARY KEY (post_id , tag_id)
);
