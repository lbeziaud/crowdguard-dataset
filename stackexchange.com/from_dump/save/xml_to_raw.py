import argparse
from lxml import etree
import re
import os

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input', help='input file (Posts.xml)', required=True)
parser.add_argument('-o', '--output', help='output dir', required=True)

args = parser.parse_args()

outputs = dict()

r_split_tags = re.compile('<(.*?)>')

context = etree.iterparse(args.input, tag='row')
for event, element in context:

    title = element.attrib.get('Title', '')
    body = element.attrib.get('Body', '')
    tags = element.attrib.get('Tags', '')
    l_tags = r_split_tags.findall(tags)

    for tag in l_tags:
        if tag not in outputs:
            tagfile = os.path.join(args.output, tag + '.raw')
            outputs[tag] = open(tagfile, 'w')
        outputs[tag].write(title + '\n')
        outputs[tag].write(body + '\n')

    element.clear()
    while element.getprevious() is not None:
        del element.getparent()[0]

for tag, tagfile in outputs.items():
    tagfile.close()
