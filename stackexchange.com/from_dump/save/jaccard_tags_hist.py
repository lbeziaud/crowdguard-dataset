import xml.etree.ElementTree as ET
import itertools
from bs4 import BeautifulSoup
import numpy as np
import random
# import pandas as pd
# import seaborn as sns
# sns.set(style="darkgrid", color_codes=True)
import matplotlib.pyplot as plt
# from skbio import DistanceMatrix
# from skbio.stats.distance import mantel

with open('1-1000.txt', 'r') as f:
    lines = f.readlines()
    stopwords = set(l.strip().lower() for l in lines)

def bigram_shingler(body):
    big_set = {body[x:x+2] for x in range(0, len(body) -1) if len(body) > 1}
    return big_set

def jacc_ind_big(bigrams1, bigrams2):
    unin = bigrams1 | bigrams2
    intn = bigrams1 & bigrams2
    if len(unin) == 0:
        return 0.0
    else:
        return len(intn) / len(unin)

def jacc_ind(set1, set2):
    intn = set1.intersection(set2)
    unin = set1.union(set2)
    if len(unin) == 0:
        return 0.0
    else:
        return len(intn) / len(unin)

se = 'english.stackexchange.com/'

def parse_posts(se):
    # posts = []
    for post in ET.parse(se + 'Posts.xml').getroot():
        score = int(post.attrib['Score'])
        if score >= 0:
            if 'Tags' in post.attrib:
                tags = set(post.attrib['Tags'].replace('<', '').split('>')[:-1])

                body = BeautifulSoup(post.attrib['Body'], 'lxml').get_text()
                body = body.strip().lower()
                body = ''.join(c for c in body if c.isalpha() or c.isspace())
                body = body.split()
                body = [w for w in body if len(w) >= 2]
                body = set(body) - stopwords
                body = ' '.join(body)

                if body:
                    shingles = bigram_shingler(body)

                    # posts.append((tags, shingles))
                    yield (tags, shingles)

posts = parse_posts('english.stackexchange.com/')
# n = len(posts)

# sim_tags = np.empty((n, n, 2), dtype=np.float)
# sim_jacc = np.empty((n, n), dtype=np.float)

# for i, j in zip(np.tril_indices(sim_tags.shape[0], -1)):
#     ti, si = posts[i]
#     tj, sj = posts[j]
#     sim_tags[i, j] = jacc_ind(ti, tj)
#     sim_jacc[i, j] = jacc_ind_big(si, sj)
    
# df = pd.DataFrame([(x, y, sim_tags[x, y], sim_jacc[x, y]) for x in range(n) for y in range(n)],
#                   columns=['x', 'y', 'tag', 'body'])
# g = sns.jointplot('tag', 'body', data=df[(df.tag > 0) & (df.body > 0)], kind='reg',
#                   xlim=(0, 1), ylim=(0, 1), color='r', size=7)
# sns.plt.show()

f, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2) # sharex=True, sharey=True

sim_tags[np.tril_indices(sim_tags.shape[0], -1)] = np.nan
sim_jacc[np.tril_indices(sim_jacc.shape[0], -1)] = np.nan

ax1.imshow(sim_tags)
ax1.set_title('tags similarity matrix')

ax2.imshow(sim_jacc)
ax2.set_title('body similarity matrix')

x = sim_tags[np.triu_indices(n)]
y = sim_jacc[np.triu_indices(n)]
ax3.scatter(x, y, marker='.')
ax3.set_xlabel('body similarity')
ax3.set_ylabel('tags similarity')

x = sim_tags[np.triu_indices(n)]
y = sim_jacc[np.triu_indices(n)]
ax4.scatter(y, x, marker='.')
ax4.set_ylabel('body similarity')
ax4.set_xlabel('tags similarity')

ax5.hist(sim_tags[np.triu_indices(n)])
ax5.set_title('tags similarity')

ax6.hist(sim_jacc[np.triu_indices(n)])
ax6.set_title('body similarity')

# sim_tags = 1 - sim_tags
# sim_jacc = 1 - sim_jacc
# x = DistanceMatrix(sim_tags)
# y = DistanceMatrix(sim_jacc)
# coeff, p_value, n = mantel(x, y)
# print('mentel: ', coeff, p_value, n)

plt.show()
