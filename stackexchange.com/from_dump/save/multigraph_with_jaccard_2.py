import numpy as np
import xml.etree.ElementTree as ET
import re
from scipy.spatial.distance import hamming
from scipy.cluster import hierarchy
import matplotlib.pyplot as plt
from collections import defaultdict
from bs4 import BeautifulSoup
import random

np.set_printoptions(threshold=np.nan)


def bigram_shingler(str):
    """Extract a set of 2 character n-grams (character bigrams) from string"""
    big_set = {str[x:x+2] for x in range(0, len(str) -1) if len(str) > 1}
    return big_set

def jacc_ind(bigrams1, bigrams2):
    """Calculate Jaccard index for 2 bigrams"""
    unin = bigrams1 | bigrams2
    intn = bigrams1 & bigrams2
    if len(unin) == 0: return 0.0
    else: return len(intn) / len(unin)

with open('1-1000.txt', 'r') as f:
    lines = f.readlines()
    stopwords = set(l.strip().lower() for l in lines)

se = 'languagelearning.stackexchange.com/'

posts = defaultdict(dict)
for post in ET.parse(se + 'Posts.xml').getroot():
    postId = int(post.attrib['Id'])
    # if 'Tags' in post.attrib:
    #     postTags = post.attrib['Tags'].replace('<', '').split('>')[:-1]
    #     for tag in postTags:
    #         posts[tag].add(postId)
    postScore = int(post.attrib['Score'])
    if postScore >= 0:
        postBody = BeautifulSoup(post.attrib['Body'], 'lxml').get_text()
        postBody = postBody.strip().lower()
        postBody = ''.join(c for c in postBody if c.isalpha() or c.isspace())
        postBody = postBody.split()
        postBody = [w for w in postBody if len(w) >= 2]
        postBody = set(postBody) - stopwords
        postBody = ' '.join(postBody)
        if postBody:
            posts[postId]['body'] = postBody
            posts[postId]['shingles'] = bigram_shingler(postBody)
            # print(postId, postBody)

n = 100
posts_subset = random.sample(list(posts.values()), n)
jac_mat = np.empty((n, n))

for i, post1 in enumerate(posts_subset):
    for j, post2 in enumerate(posts_subset):
        jac_sims = jacc_ind(post1['shingles'], post2['shingles'])
        # print(id1, id2, jac_sims)
        jac_mat[i, j] = jac_sims

# jac_mat[jac_mat < jac_mat.mean()] 

# plt.imshow(jac_mat)
# plt.show()

import networkx as nx

G = nx.from_numpy_matrix(jac_mat)

# for u, v, d in G.edges(data='weight'):
#     if d < jac_mat.mean():
#         print(u, v, d)
#         G.remove_edge(u, v)

nx.draw(G,pos=nx.spring_layout(G))

plt.show()
