import re
from lxml import etree
from bs4 import BeautifulSoup
import MySQLdb
import itertools


fn_xml_tags = '/home/louis/Documents/crowdguard-dataset/src_may03/english.stackexchange.com/Tags.xml'
fn_xml_posts = '/home/louis/Documents/crowdguard-dataset/src_may03/english.stackexchange.com/Posts.xml'


def parse_tags(fn):
    for event, element in etree.iterparse(fn, tag='row'):
        yield (
            element.attrib.get('Id'),
            element.attrib.get('TagName'),
        )
        element.clear()


def parse_posts(fn):
    r = re.compile('<(.*?)>')
    for event, element in etree.iterparse(fn, tag='row'):
        tags = r.findall(element.attrib.get('Tags', ''))
        body = ''.join([x for x in ''.join(BeautifulSoup(element.attrib.get('Body'), 'lxml').get_text()) if ord(x) < 128])
        yield (
            element.attrib.get('Id'),
            element.attrib.get('PostTypeId'),
            element.attrib.get('ParentId'),
            element.attrib.get('acceptedAnswerId'),
            element.attrib.get('Score'),
            element.attrib.get('ViewCount'),
            body,
            element.attrib.get('Title'),
            element.attrib.get('AnswerCount'),
            element.attrib.get('CommentCount'),
            element.attrib.get('FavoriteCount'),
            tags,
        )
        element.clear()


def grouper(n, iterable):
    it = iter(iterable)
    while True:
        chunk_it = itertools.islice(it, n)
        try:
            first_el = next(chunk_it)
        except StopIteration:
            return
        yield itertools.chain((first_el,), chunk_it)


db = MySQLdb.connect(host='localhost', user='root', passwd='f6QJeo({,xT~OH6})E=^;.', db='stackoverflow')
db.set_character_set('utf8')
cur = db.cursor()
cur.execute('SET GLOBAL max_allowed_packet=1073741824')

# tags
data_tags = parse_tags(fn_xml_tags)
for chunk in grouper(10000, data_tags):
    cur.executemany("""INSERT INTO tags (id, tag_name) VALUES (%s, %s)""", chunk)
db.commit()

data_posts, data_posttags = itertools.tee(parse_posts(fn_xml_posts))

# send posts
data_posts = (row[:-1] for row in data_posts)
for chunk in grouper(10000, data_posts):
    cur.executemany("""INSERT INTO posts (id, post_type_id, parent_id, accepted_answer_id, score, view_count, body, title, answer_count, comment_count, favorite_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", chunk)
db.commit()

# send posttags
def posttags(rows):
    for row in rows:
        post_id, tag_names = row
        for tag_name in tag_names:
            cur.execute("""SELECT id FROM tags WHERE tag_name=%s""", (tag_name,))
            tag_id = cur.fetchone()[0]
            yield (post_id, tag_id)
data_posttags = posttags((row[0], row[-1]) for row in data_posttags)
for chunk in grouper(10000, data_posttags):
    cur.executemany("""INSERT INTO post_tags (post_id, tag_id) VALUES (%s, %s)""", chunk)
db.commit()

db.close()
