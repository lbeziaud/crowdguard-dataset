import lxml.etree as ET
import re
import sys
import sqlite3
import os

dirs = sys.argv[1:]

conn = sqlite3.connect('posts.db')

c = conn.cursor()

c.execute('''
CREATE TABLE IF NOT EXISTS site (
name TEXT PRIMARY KEY
)
''')

c.execute('''
CREATE TABLE IF NOT EXISTS tag (
id INTEGER, 
siteName TEXT, 
name TEXT, 
PRIMARY KEY (id, siteName), 
FOREIGN KEY(siteName) REFERENCES site(name)
)
''')

c.execute('''
CREATE TABLE IF NOT EXISTS post (
id INTEGER, 
siteName TEXT, 
parentId INTEGER, 
score INTEGER, 
body TEXT, 
PRIMARY KEY (id, siteName), 
FOREIGN KEY(siteName) REFERENCES site(name), 
FOREIGN KEY(parentId) REFERENCES post(id)
)
''')

c.execute('''
CREATE TABLE IF NOT EXISTS post2tag (
postId INTEGER, 
tagId INTEGER, 
siteName TEXT, 
PRIMARY KEY (postId, tagId, siteName), 
FOREIGN KEY(siteName) REFERENCES site(name), 
FOREIGN KEY(postId) REFERENCES post(id), 
FOREIGN KEY(tagId) REFERENCES tag(id)
)
''')

r = re.compile('<(.*?)>')

for path in dirs:
    if not os.path.isfile(path + '/Tags.xml') or not os.path.isfile(path + '/Posts.xml'):
        continue

    siteName = os.path.basename(path)
    print(path, siteName)

    c.execute('INSERT OR IGNORE INTO site VALUES (?)', (siteName,))

    for event, elem in ET.iterparse(path + '/Tags.xml'):
        if event == 'end':
            if elem.tag == 'row':
                tagId = elem.get('Id')
                tagName = elem.get('TagName')
                c.execute('INSERT OR IGNORE INTO tag VALUES (?, ?, ?)', (tagId, siteName, tagName))
        elem.clear()
    conn.commit()

    for event, elem in ET.iterparse(path + '/Posts.xml'):
        if event == 'end':
            if elem.tag == 'row':
                postId = elem.get('Id')
                parentId = elem.get('ParentId')
                score = elem.get('Score')
                body = elem.get('Body')
                
                c.execute('INSERT OR IGNORE INTO post VALUES (?, ?, ?, ?, ?)', (postId, siteName, parentId, score, body))

                for tagName in r.findall(elem.get('Tags', '')):
                    c.execute('SELECT id FROM tag WHERE name = ? AND siteName = ?', (tagName,siteName))
                    tagId = c.fetchone()[0]
                    c.execute('INSERT OR IGNORE INTO post2tag VALUES (?, ?, ?)', (postId, tagId, siteName))
        elem.clear()
    conn.commit()

conn.close()
