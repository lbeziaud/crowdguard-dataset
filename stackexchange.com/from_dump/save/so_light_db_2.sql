CREATE DATABASE se_english 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

USE se_english;

CREATE TABLE post
  (
     id                 INT PRIMARY KEY,
     post_type_id       TINYINT,
     parent_id          INT,
     accepted_answer_id INT,
     score              INT,
     view_count         INT,
     body               TEXT,
     title              NVARCHAR(250),
     answer_count       INT,
     comment_count      INT,
     favorite_count     INT
  );

CREATE TABLE tag
  (
     id       INT PRIMARY KEY,
     tag_name NVARCHAR(35)
  );

CREATE TABLE post_tag
  (
     post_id INT,
     tag_id  INT,
     PRIMARY KEY (post_id, tag_id)
  -- FOREIGN KEY (post_id) REFERENCES post(id)
  -- FOREIGN KEY (tag_id)  REFERENCES tag(id)
  );  