import MySQLdb
import re
from lxml import etree
from bs4 import BeautifulSoup
import itertools

db = MySQLdb.connect(host='192.168.0.27', user='root', passwd='toto', db='stackoverflow')
db.set_character_set('utf8mb4')
cur = db.cursor()
# need to be able to send big packets
cur.execute('SET GLOBAL max_allowed_packet=1073741824')

def parse_tags(fn):
    for event, element in etree.iterparse(fn, tag='row'):
        yield (
            element.attrib.get('Id'),
            element.attrib.get('TagName'),
        )
        element.clear()

def parse_posts(fn):
    r = re.compile('<(.*?)>')
    for event, element in etree.iterparse(fn, tag='row'):
        # '<tag1><tag2>' to ['tag1', 'tag2']
        tags = r.findall(element.attrib.get('Tags', ''))
        # remove html markup and non utf8
        body = element.attrib.get('Body')
        body = BeautifulSoup(body, 'lxml').get_text()
        body = body.encode('utf8', errors='ignore')
        yield (
            element.attrib.get('Id'),
            element.attrib.get('PostTypeId'),
            element.attrib.get('ParentId'),
            element.attrib.get('AcceptedAnswerId'),
            element.attrib.get('Score'),
            body,
            element.attrib.get('Title'),
            tags,
        )
        element.clear()

def grouper(n, iterable):
    # Collect data into chunks of given length
    it = iter(iterable)
    while True:
        chunk_it = itertools.islice(it, n)
        try:
            first_el = next(chunk_it)
        except StopIteration:
            return
        yield itertools.chain((first_el,), chunk_it)

chunk_size = 10000 # max number of rows in insert transactions

tags = parse_tags('/media/louis/rugged/stackexchange/stackoverflow.com/Tags.xml')
sql = 'INSERT IGNORE INTO tag VALUES (%s, %s)'
i = chunk_size
for tags_chunk in grouper(chunk_size, tags):
   cur.executemany(sql, tags_chunk)
   print(i)
   i += chunk_size
db.commit()

# duplicate the generator
posts, post_tags = itertools.tee(parse_posts('/media/louis/rugged/stackexchange/stackoverflow.com/Posts.xml'))

posts = (post[:-1] for post in posts) # remove tags from tuple
sql = 'INSERT IGNORE INTO post VALUES (%s, %s, %s, %s, %s, %s, %s)'
i = chunk_size
for posts_chunk in grouper(chunk_size, posts):
   cur.executemany(sql, posts_chunk)
   print(i)
   i += chunk_size
db.commit()

def f_post_tags(posts):
   for post in posts:
       post_id, tag_names = post
       for tag_name in tag_names:
           cur.execute('SELECT id FROM tag WHERE tag_name=%s', (tag_name,))
           tag_id = cur.fetchone()[0]
           yield (post_id, tag_id)

post_tags = f_post_tags((post[0], post[-1]) for post in post_tags)
sql = 'INSERT IGNORE INTO post_tag VALUES (%s, %s)'
i = chunk_size
for post_tags_chunk in grouper(chunk_size, post_tags):
   cur.executemany(sql, post_tags_chunk)
   print(i)
   i += chunk_size
db.commit()

db.close() # close connexion to db
