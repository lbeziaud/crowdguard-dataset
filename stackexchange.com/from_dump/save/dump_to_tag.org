#+TITLE: Building a Taxonomy from Stack Exchange's Data
#+AUTHOR: Louis Béziaud

#+BEGIN_SRC python
import argparse
import glob
import html
import json
from lxml import etree
import networkx as nx
from networkx.readwrite import json_graph
import os
import pandas as pd
import pickle
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
#+END_SRC

#+BEGIN_SRC python
def xml_to_raw(dir):
    input = os.path.join(dir, 'Posts.xml')
    outputs = dict()
    if not os.path.exists(os.path.join(dir, 'raw/')):
        os.makedirs(os.path.join(dir, 'raw/'))
    r_split_tags = re.compile('<(.*?)>')
    context = etree.iterparse(input, tag='row')
    for event, element in context:
        title = element.attrib.get('Title', '')
        body = element.attrib.get('Body', '')
        tags = element.attrib.get('Tags', '')
        l_tags = r_split_tags.findall(tags)
        for tag in l_tags:
            if tag not in outputs:
                tagfile = os.path.join(dir, 'raw/', tag + '.raw')
                outputs[tag] = open(tagfile, 'w')
            outputs[tag].write(title + '\n')
            outputs[tag].write(body + '\n')
        element.clear()
        while element.getprevious() is not None:
            del element.getparent()[0]
    for tag, tagfile in outputs.items():
        tagfile.close()
#+END_SRC

#+BEGIN_SRC python
def raw_to_txt(dir):
    if not os.path.exists(os.path.join(dir, 'txt/')):
        os.makedirs(os.path.join(dir, 'txt/'))
    cleanr = re.compile('<.*?>')
    for in_fn in glob.glob(os.path.join(dir, 'raw/', '*.raw')):
        with open(in_fn, 'r') as in_f:
            base = os.path.basename(in_fn)
            tag_name = os.path.splitext(base)[0]
            out_fn = os.path.join(dir, 'txt/', tag_name + '.txt')
            with open(out_fn, 'w') as out_f:
                lines = in_f.readlines()
                for line in lines:
                    line = html.unescape(line) # unescape html &lt;
                    line = re.sub(cleanr, '', line) # remove html <tags>
                    out_f.write(line)
#+END_SRC

#+BEGIN_SRC python
def txt_to_sim(dir):
    corpus = glob.glob(os.path.join(dir, 'txt/', '*.txt'))
    tags = [os.path.splitext(os.path.basename(f))[0] for f in corpus]
    tf = TfidfVectorizer(analyzer='word', stop_words='english')
    tfidf = tf.fit_transform(corpus)
    cosine_sim = linear_kernel(tfidf, tfidf)
    df = pd.DataFrame(cosine_sim, columns=tags, index=tags)
    fn = os.path.join(dir, 'sim.pkl')
    df.to_pickle(fn)
#+END_SRC

#+BEGIN_SRC python
def sim_to_taxo(dir, threshold):
    def heymann2006collaborative(threshold, msim):
        tags = msim.columns  # set of tags
        taxonomy = nx.DiGraph()  # directed graph
        taxonomy.add_node('root')
        for ti in tags:
            # find most outer node
            max_candidate = None
            max_candidate_val = -1
            for tj in taxonomy:
                if tj == 'root': continue
                sim = msim[ti][tj]
                if sim > max_candidate_val:
                    max_candidate_val = sim
                    max_candidate = tj
            # add node to the taxonomy
            if max_candidate_val > threshold:
                taxonomy.add_edge(max_candidate, ti)
            else:
                taxonomy.add_edge('root', ti)
        taxonomy = nx.convert_node_labels_to_integers(taxonomy, label_attribute='name')
        return taxonomy

    sim_fn = os.path.join(dir, 'sim.pkl')
    msim = pd.read_pickle(sim_fn)
    taxonomy = heymann2006collaborative(threshold, msim)
    root = next((n for n in taxonomy if taxonomy.node[n]['name'] == 'root')) # TODO: opti
    js = json_graph.tree_data(taxonomy, root=root)
    out_fn = os.path.join(dir, 'taxonomy_{}.json'.format(threshold))
    with open(out_fn, 'w') as f:
        f.write(json.dumps(js))
#+END_SRC

#+BEGIN_SRC python
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', help='working directory', required=True)
    parser.add_argument('-t', '--threshold', help='taxonomy threshold', required=True, type=float)
    args = parser.parse_args()
    xml_to_raw(args.dir)
    raw_to_txt(args.dir)
    txt_to_sim(args.dir)
    sim_to_taxo(args.dir, args.threshold)
#+END_SRC
