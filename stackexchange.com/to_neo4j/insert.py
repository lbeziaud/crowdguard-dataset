from neo4j.v1 import GraphDatabase, basic_auth
from lxml import etree
import re

driver = GraphDatabase.driver('bolt://localhost:7687', auth=basic_auth('neo4j', '65N1wV1qIYD42jeNfVfwtw'))
session = driver.session()

session.run("CREATE INDEX ON :Tag(name)")
session.run("CREATE INDEX ON :Post(id)")

tags_fn = '../english.stackexchange.com/Tags.xml'
for event, element in etree.iterparse(tags_fn, tag='row'):
    tag_name = element.attrib.get('TagName')

    session.run("CREATE (a:Tag {name: {tag_name}})",
                {"tag_name": tag_name})

    element.clear()
    while element.getprevious() is not None:
        del element.getparent()[0]

        
r_tag = re.compile('<(.*?)>')
posts_fn = '../english.stackexchange.com/Posts.xml'
for event, element in etree.iterparse(posts_fn, tag='row'):
    post_type = element.attrib.get('PostTypeId')
    if post_type != '1':
        # not a question
        continue

    post_id = int(element.attrib.get('Id'))
    post_title = element.attrib.get('Title')
    post_body = element.attrib.get('Body')
    post_score = int(element.attrib.get('Score'))

    session.run("CREATE (a:Post {id: {post_id}, title: {post_title}, body: {post_body}, score: {post_score}})",
                {'post_id': post_id, 'post_title': post_title,
                 'post_body': post_body, 'post_score': post_score})

    post_tags = element.attrib.get('Tags')
    for tag_name in r_tag.findall(post_tags):
        session.run("""MATCH (p: Post {id: {post_id}}), (t: Tag {name: {tag_name}})
                       CREATE (p)-[:TAGGED]->(t)""",
                    {'post_id': post_id, 'tag_name': tag_name})

    element.clear()
    while element.getprevious() is not None:
        del element.getparent()[0]


session.close()
