from neo4j.v1 import GraphDatabase, basic_auth
import re
import html
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import networkx as nx


driver = GraphDatabase.driver('bolt://127.0.0.1:7687', auth=basic_auth('neo4j', '65N1wV1qIYD42jeNfVfwtw'))
session = driver.session()

result = session.run("""
MATCH (p:Post)-[r:TAGGED]->(t:Tag)
WITH p, r, t
WITH t.name AS tag, COLLECT(p.title + ' ' + p.body) AS posts
RETURN tag, posts
LIMIT 20
""")

# result = session.run("""MATCH (t:Tag) RETURN t.name as name""")

for record in result:
    print(record['tag'])

session.close()
# corpus = ((record['tag'], record['posts']) for record in result)
# corpus = pd.DataFrame(corpus, columns=['tag', 'posts'])
# print(corpus)

