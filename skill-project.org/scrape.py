import requests
import networkx as nx
from networkx.readwrite import json_graph
from collections import defaultdict
import json

def tree(): return defaultdict(tree)
def dicts(t): return {k: dicts(t[k]) for k in t}

api = 'http://en.skill-project.org/api/'
rootId = '5430126ba0a292f58144425'

taxonomy = nx.DiGraph()

taxonomy.add_node(rootId, name='Skills')

def getNodeChildren(nodeId, depth=0, max_depth=None):
    url = api + 'getNodeChildren/'
    headers = {'X-Requested-With': 'XMLHttpRequest'}
    response = requests.get(url + nodeId, headers=headers)
    fixtures = response.json()

    print(nodeId)

    if max_depth and depth >= max_depth:
        return
        
    for child in fixtures['data']:
        childId = child['uuid']
        childName = child['name']

        taxonomy.add_node(childId, name=childName)
        taxonomy.add_edge(nodeId, childId)

        getNodeChildren(childId, depth=depth+1, max_depth=max_depth)
    
getNodeChildren(rootId)

with open('taxonomy.json', 'w') as fp:
    data = json_graph.tree_data(taxonomy, root=rootId)
    json.dump(data, fp)
